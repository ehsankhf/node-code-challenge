# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

```
The output is "2" as the setTimeout gets executed after 100ms, 
and the second console get executed much sooner than that
```

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

```
It's simply a recursive function that is returning the values from 10..0
```

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

```
I cannot see the issue
```

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

```
It will be 3 due to the closure defined for the returned function, 
which is able to access `a` in the outer scope. 
```

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

```
It's simply using the callback pattern, and will call the callback after 100ms
```
