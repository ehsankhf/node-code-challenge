# Node code challenge

## How to run:
1. `npm i`
2. `npm start`
3. Browse `localhost:3000`

## How to test:
1. `npm i`
2. `npm t`

## TODOs

 * Display coordinates next to results
 * Sort the results by the closest name match
