const sqlite3 = require('sqlite3');
const express = require("express");
const {setupRoutes} = require("./modules/routes");
const app = express();

const HTTP_PORT = process.env.PORT || 3000;

async function bootstrapServer() {
    app.use(async (req,res, next)=>{
        const db = await new Promise((resolve, reject) => {
            const db = new sqlite3.Database('./data/test.db', (err) => {
                if (err) {
                    return reject(err);
                }
            });

            return resolve(db);
        });

        req.db = db;
        next();
    });

    app.use(express.static('public'));
    setupRoutes(app);
    app.use((err, req, res, next) => {
        console.error(err);
        return res.status(500).json({
            status: 'error',
            message: err.message
        });
    })

    await new Promise(resolve => app.listen(HTTP_PORT, () => {
        console.log(`App listening at http://localhost:${HTTP_PORT}`)
        resolve();
    }));

    return {
        app
    };
}

module.exports = {
    bootstrapServer
};
