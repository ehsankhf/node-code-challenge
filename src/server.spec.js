const supertest = require('supertest');
const {bootstrapServer} = require("./server");

describe('Server', () => {
    let app, db, server;
    beforeAll(async () => {
        ({app, db} = await bootstrapServer());
        server = supertest(app)
    });

    afterAll(() => {
        server.close();
    });

    describe('GET /location', () => {
        describe('When the given string is similar to any location name', () => {
            it('Should return all matching location names', async () => {
                const result = await server.get('/location?q=asting');
                expect(result.body).toHaveLength(23);

                for (let location of result.body) {
                    expect(location).toBeDefined();
                    expect(typeof location).toEqual('string');
                }
            });
        });

        describe('When the given string is NOT similar to any location name', () => {
            it('Should return an empty response', async () => {
                const result = await server.get('/location?q=ABCD');
                expect(result.body).toHaveLength(0);
            })
        });

        describe('When the given string includes non-letter characters', () => {
            it('Should return an empty response', async () => {
                const result = await server.get('/location?q=ABCD');
                expect(result.body).toHaveLength(0);
            });
        });
    });
});
