const {LocationController} = require("./location/controller");

function setupRoutes(app) {
    app.get('/location', LocationController.getLocations);

    return app;
}

module.exports = {
    setupRoutes
};
