class LocationController {
    static async getLocations(req, res, next) {
        const location = req.query.q;

        let result = [];

        if (/^[a-zA-Z\s]+$/.test(location.trim())) {
            result = await new Promise(resolve => req.db.all(
                `SELECT LOWER(asciiname) AS location
                 FROM locations
                 WHERE LOWER(asciiname) LIKE '%' || ? || '%' `,
                [location], (err, data) => {
                    if (err) {
                        return next(err);
                    }
                    resolve(data)
                }
            ));
        }

        res.json(result.map(({location}) => location) || []);
    }
}

module.exports = {
    LocationController
};
